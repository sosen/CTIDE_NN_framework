import ROOT
import argparse
import root_numpy as rnp

def main():
  parse = argparse.ArgumentParser()
  parse.add_argument('path')
  parse.add_argument('--rescaledIBL', action='store_true', default=False)
  parse.add_argument('--ncluster', default=-1)
  args = parse.parse_args()
  print "root file: ", args.path, "\t iblrescaled =", args.rescaledIBL
  if (args.rescaledIBL): nbitIBL = 8
  else: nbitIBL = 4
  f = ROOT.TFile(args.path, 'READ')
  t = f.Get("NNinput")
  N=int(args.ncluster)
  print N
  if (N==-1):
    nevents = t.GetEntries()
  else: 
    nevents = min(t.GetEntries(), N)
  print nevents
  #might be inefficient to store the whole tree at once, if the required clusters is less.
  #Have to go by event. change later
  barrelEC = rnp.tree2array(t, "NN_barrelEC")
  layer = rnp.tree2array(t, "NN_layer")
  particles = [rnp.tree2array(t, "NN_nparticles1"), rnp.tree2array(t, "NN_nparticles2"), rnp.tree2array(t, "NN_nparticles3")]
  ClusterToT = []
  for pix in range(49):
    ClusterToT.append([])
    ClusterToT[-1] = rnp.tree2array(t, "NN_matrix%i"%pix)
  hIBL = [ROOT.TH1F('IBLToT', 'IBLToT', 1000, 0, (2**nbitIBL)+3)]
  hBarrel = [ROOT.TH1F('BarrelToT', 'BarrelToT', 1000, 0, (2**8)+3)]
  hEndcap = [ROOT.TH1F('EndcapToT', 'EndcapToT', 1000, 0, (2**8)+3)]
  for i in range(3):
    m = ((i%3)+1)
    n = (((i+1)%3)+1)
    hIBL.append(ROOT.TH1F('IBLToT_%ip%ip'%(m, n) , 'IBLToT_%ip%ip'%(m, n), 1000, 0, (2**nbitIBL)+3))
    hBarrel.append(ROOT.TH1F('BarrelToT_%ip%ip'%(m, n) , 'BarrelToT_%ip%ip'%(m, n), 1000, 0, (2**8)+3))
    hEndcap.append(ROOT.TH1F('EndcapToT_%ip%ip'%(m, n) , 'EndcapToT_%ip%ip'%(m, n), 1000, 0, (2**8)+3))

  for event in range(nevents):
    if ((barrelEC[event] == 0) and (layer[event] == 0)): #IBL
      for pix1 in range(49):
        hIBL[0].Fill(ClusterToT[pix1][event])
      for p in range(3):
	m = ((p%3)+1)
	n = (((p+1)%3)+1)
	if ((particles[m-1][event]) or (particles[n-1][event])):
          for pix in range(49):
	    hIBL[m].Fill(ClusterToT[pix][event])
      
    if ((barrelEC[event] == 0) and (layer[event] != 0)): #Barrel
      for pix1 in range(49):
        hBarrel[0].Fill(ClusterToT[pix1][event])
      for p in range(3):
	m = ((p%3)+1)
	n = (((p+1)%3)+1)
	if ((particles[m-1][event]) or (particles[n-1][event])):
          for pix in range(49):
            hBarrel[m].Fill(ClusterToT[pix][event])
	  
    if (barrelEC[event] != 0): #Endcap
      for pix1 in range(49):
        hEndcap[0].Fill(ClusterToT[pix1][event])
      for p in range(3):
	m = ((p%3)+1)
	n = (((p+1)%3)+1)
	if ((particles[m-1][event]) or (particles[n-1][event])):
          for pix in range(49):
            hEndcap[m].Fill(ClusterToT[pix][event])

  tag = args.path
  tag = tag.split(".")
  if (tag[-2] == "NNinput"):
    tag = "test"
  else:
    tag = "test"+tag[-2]

  out_dir = "."

  cibl0 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cibl0.SetLogy()
  hIBL[0].SetTitle("IBL ToT")
  hIBL[0].Draw('hist')
  imgibl0=ROOT.TImage.Create()
  imgibl0.FromPad(cibl0)
  imgibl0.WriteImage(out_dir+"/IBLToT%s.png"%(tag))

  cibl1 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cibl1.SetLogy()
  hIBL[1].SetTitle("IBL_1p_2p ToT")
  hIBL[1].Draw('hist')
  imgibl1=ROOT.TImage.Create()
  imgibl1.FromPad(cibl1)
  imgibl1.WriteImage(out_dir+"/IBLToT_1p2p%s.png"%(tag))

  cibl2 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cibl2.SetLogy()
  hIBL[2].SetTitle("IBL_2p_3p ToT")
  hIBL[2].Draw('hist')
  imgibl2=ROOT.TImage.Create()
  imgibl2.FromPad(cibl2)
  imgibl2.WriteImage(out_dir+"/IBLToT_2p3p%s.png"%(tag))

  cibl3 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cibl3.SetLogy()
  hIBL[3].SetTitle("IBL_3p_1p ToT")
  hIBL[3].Draw('hist')
  imgibl3=ROOT.TImage.Create()
  imgibl3.FromPad(cibl3)
  imgibl3.WriteImage(out_dir+"/IBLToT_3p1p%s.png"%(tag))

##Barrel
  cbarrel0 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cbarrel0.SetLogy()
  hBarrel[0].SetTitle("Barrel ToT")
  hBarrel[0].Draw('hist')
  imgbarrel0=ROOT.TImage.Create()
  imgbarrel0.FromPad(cbarrel0)
  imgbarrel0.WriteImage(out_dir+"/BarrelToT%s.png"%(tag))

  cbarrel1 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cbarrel1.SetLogy()
  hBarrel[1].SetTitle("Barrel_1p_2p ToT")
  hBarrel[1].Draw('hist')
  imgbarrel1=ROOT.TImage.Create()
  imgbarrel1.FromPad(cbarrel1)
  imgbarrel1.WriteImage(out_dir+"/BarrelToT_1p2p%s.png"%(tag))

  cbarrel2 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cbarrel2.SetLogy()
  hBarrel[2].SetTitle("Barrel_2p_3p ToT")
  hBarrel[2].Draw('hist')
  imgbarrel2=ROOT.TImage.Create()
  imgbarrel2.FromPad(cbarrel2)
  imgbarrel2.WriteImage(out_dir+"/BarrelToT_2p3p%s.png"%(tag))

  cbarrel3 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cbarrel3.SetLogy()
  hBarrel[3].SetTitle("Barrel_3p_1p ToT")
  hBarrel[3].Draw('hist')
  imgbarrel3=ROOT.TImage.Create()
  imgbarrel3.FromPad(cbarrel3)
  imgbarrel3.WriteImage(out_dir+"/BarrelToT_3p1p%s.png"%(tag))

##Endcap
  cendcap0 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cendcap0.SetLogy()
  hEndcap[0].SetTitle("Endcap ToT")
  hEndcap[0].Draw('hist')
  imgendcap0=ROOT.TImage.Create()
  imgendcap0.FromPad(cendcap0)
  imgendcap0.WriteImage(out_dir+"/EndcapToT%s.png"%(tag))

  cendcap1 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cendcap1.SetLogy()
  hEndcap[1].SetTitle("Endcap_1p_2p ToT")
  hEndcap[1].Draw('hist')
  imgendcap1=ROOT.TImage.Create()
  imgendcap1.FromPad(cendcap1)
  imgendcap1.WriteImage(out_dir+"/EndcapToT_1p2p%s.png"%(tag))

  cendcap2 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cendcap2.SetLogy()
  hEndcap[2].SetTitle("Endcap_2p_3p ToT")
  hEndcap[2].Draw('hist')
  imgendcap2=ROOT.TImage.Create()
  imgendcap2.FromPad(cendcap2)
  imgendcap2.WriteImage(out_dir+"/EndcapToT_2p3p%s.png"%(tag))

  cendcap3 = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
  cendcap3.SetLogy()
  hEndcap[3].SetTitle("Endcap_3p_1p ToT")
  hEndcap[3].Draw('hist')
  imgendcap3=ROOT.TImage.Create()
  imgendcap3.FromPad(cendcap3)
  imgendcap3.WriteImage(out_dir+"/EndcapToT_3p1p%s.png"%(tag))






#cibl1 = 
#  printhist(hIBL[0], 'IBLToT')
#  printhist(hBarrel[0], 'BarrelToT')
#  printhist(hEndcap[0], 'EndcapToT')
#  for k in range(3):
#    m = ((k%3)+1)
#    n = (((k+1)%3)+1)
#    printhist(hIBL[m], 'IBLToT_%ip%ip'%(m,n))
#    printhist(hBarrel[m], 'BarrelToT_%ip%p'%(m,n))
#    printhist(hEndcap[m], 'EndcapToT_%ip%p'%(m,n))
#
#def printhist(hist, name):
#  c = ROOT.TCanvas(name, name, 0, 0, 800, 600)
#  c.SetLogy()
#  hist.Draw('hist')
#  out_dir = "../plot_dump"
#  img=ROOT.TImage.Create()
#  img.FromPad(c)
#  img.WriteImage(out_dir+"/"+name+".png")

if __name__ == '__main__':
  main()
