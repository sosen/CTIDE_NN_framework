void processToT(const char *fin, const char *fout, string process, int ncluster, int nbits = 8)
{   
  //bit_res = int(pow(2,bit_res));
  TFile *f = new TFile(fin);
  TTree *t = (TTree*)f->Get("NNinput");

  Long64_t nevents = t->GetEntries();

  cout << nevents << endl;

  if ((ncluster > nevents)||(ncluster == -1)) {ncluster = nevents;}

  //copying the data file
  t->SetBranchStatus("*", 1);
  for ( int pix = 0; pix < 49; pix++ ) {
    char key[256];
    std::sprintf(key, "NN_matrix%d", pix);
    t->SetBranchStatus(key, 0);
  }   
  TFile *f1 = new TFile(fout, "recreate");
  TTree *t1 = t->CloneTree(ncluster);

  int bit_res = 1;
  if (nbits != 8) {
    bit = nbits;
    int original_bit = 8; //original memory 8 bits
    bit_res = pow(2, (original_bit - bit));
  }
  cout << "nbits " << nbits << endl;
  cout << bit_res << endl;

  Double_t ChargeNbit[49];
  Double_t pixelcharge[49];
  TBranch *NN_Charge[49];
  t->SetBranchStatus("*", 0);
  for ( int pix = 0; pix < 49; pix++ ) {
    char key[256], type[256];
    std::sprintf(key, "NN_matrix%d", pix);
    std::sprintf(type, "NN_matrix%d/D", pix);
    t->SetBranchStatus(key, 1);
    t->SetBranchAddress(key, &pixelcharge[pix]);
    NN_Charge[pix] = t1->Branch(key, &ChargeNbit[pix], type);
  }
  Double_t barrel, layer;
  t->SetBranchStatus("NN_layer", 1);
  t->SetBranchAddress("NN_layer", &layer);
  t->SetBranchStatus("NN_barrelEC", 1);
  t->SetBranchAddress("NN_barrelEC", &barrel);
  cout << process << endl;
  for ( Long64_t i=0; i<ncluster; i++ ) {
    t->GetEntry(i);
    //
    if (process == "iblrescale"){
      if (barrel == 0 && layer == 0){
        for ( int pix = 0; pix < 49; pix++ ) {
          ChargeNbit[pix] = pixelcharge[pix]*16;
          NN_Charge[pix]->Fill();
        }
      }
      else {
        for ( int pix = 0; pix < 49; pix++ ) {
          ChargeNbit[pix] = pixelcharge[pix];
          NN_Charge[pix]->Fill();
        }
      }
    }
    //
    if (process == "scheme2"){
      for ( int pix = 0; pix < 49; pix++ ) {
        ChargeNbit[pix] = bit_res*int(pixelcharge[pix]/bit_res); //scheme 2 (8bit -> 4 bit: (0-15 -> 0, 16-31-> 16, ...)
        NN_Charge[pix]->Fill();
      }
    }
    //
    if (process == "scheme1"){
      for ( int pix = 0; pix < 49; pix++ ) {
        if (pixelcharge[pix] != 0) { //scheme 1
          ChargeNbit[pix] = bit_res*int(pixelcharge[pix]/bit_res) + bit_res/2; //scheme 1
        } // scheme 1 (8bit -> 4 bit: 0 -> 0, 1-15 -> 7, 16-31 -> 23, ...)
        else {
          ChargeNbit[pix]=0;
	} //scheme 1
	NN_Charge[pix]->Fill();
      }
    }
    //
  }

  t1->Print();
  f1->Write();
}
