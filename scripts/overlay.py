import ROOT
import subprocess
import argparse
def plot(p, n, layer, file_list, legend_list, out_dir):
   c = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
   #c.SetLogy()
   hist="ROC_%ivs%i_%s_all"%(p,n,layer)
   # legend
   leg = ROOT.TLegend( 0.65, 0.75, 0.88, 0.90 )
   leg.SetFillColor(0)
   leg.SetLineColor(0)
   for i in range(len(file_list)):
     f = ROOT.TFile(file_list[i])
     g = f.Get(hist)
   #  print i+3
     g.SetMarkerSize(0.05)
     if (i == 0):
       g.SetLineColor(1)
       g.SetTitle(';False positive rate P(%i|%i);False Negative Rate P(%i|%i);'%(p,n,n,p))
       g.Draw()
     else:
       if (i != 2): g.SetLineColor(i+3) #avoiding yellow
       else: g.SetLineColor(ROOT.kGreen)
       g.Draw("same")
     leg.AddEntry(g, legend_list[i], "L")
     f.Close()

   leg.Draw()
   img=ROOT.TImage.Create()
   img.FromPad(c)
   #out=hist+".png" #".eps"
   #c.Print(out)
   #subprocess.call(['ps2pdf', '-dEPSCrop', out])
   #subprocess.call(['rm', out])
   img.WriteImage(out_dir+"/"+hist+".png")
 
def main():
   parse = argparse.ArgumentParser()
   parse.add_argument('-f', action='append', required=True)
   parse.add_argument('-l', action='append', required=True)
   parse.add_argument('--outdirname', required=True)
   args = parse.parse_args()
   file_list = args.f
   legend_list = args.l
   out_dir = args.outdirname
   subprocess.call(['mkdir', out_dir])
   # the above part need more work

   if (len(file_list) < 2):
     print "atleast two files needed"
     return
   elif (len(file_list) != len(legend_list)):
     print "provide legend for each file next time"
     return
   print file_list, legend_list
   layers = ['all', 'barrel', 'endcap', 'ibl']
   for p in range(1,4):
     for n in range(1,4):
       for layer in layers:
         if(p!=n): plot(p, n, layer, file_list, legend_list, out_dir)
 
if __name__ == '__main__':
    main()
