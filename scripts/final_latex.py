#import sys
import subprocess

def printinfile(list_):
  f = open('results.tex', 'a')
  for l in list_:
    f.write(l)
  f.close()
  return


def main():
  testname = "test" #sys.argv[1]
  f = open('results.tex', 'w')
  f.close()
  start = ['\\documentclass[a4paper,12pt]{article}\n', '\\usepackage{lscape}\n', '\\usepackage{graphicx}\n', '\\usepackage{amsmath}\n', '\\usepackage{subcaption}\n', '\\usepackage{color}\n', '\\begin{document}\n']
  printinfile(start)

  front_page = ["\\title{ToT nbits study update}\n", "\\author{Sourav Sen \\\\(Duke)}\n", "\\date{\\today \\\\ CTIDE weekly meeting}\n", "\\maketitle\n", "\\clearpage\n"]
  printinfile(front_page)

  printinfile(['\\begin{landscape}\n'])
  OriToT = ["\\section*{Original ToT distribution}\n", '\\begin{figure*}[htb]\n', '\\centering\n', '\\begin{subfigure}[t]{0.4\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{IBLToT%s.png} \n'%(testname), '\\caption{IBL ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.4\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{BarrelToT%s.png} \n'%(testname), '\\caption{Barrel ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.4\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{EndcapToT%s.png} \n'%(testname), '\\caption{Endcap ToT}\n', '\\end{subfigure}\n', '\\end{figure*}\n']
  printinfile(OriToT)

  RescaledToT = ["\\section*{ToT distribution after IBL ToT rescaling (multiplied by a factor of 16)}\n", '\\begin{figure*}[htb]\n', '\\centering\n', '\\begin{subfigure}[t]{0.4\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{IBLToT%s.png} \n'%(testname+"ibl_rescaled"), '\\caption{IBL ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.4\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{BarrelToT%s.png} \n'%(testname+"ibl_rescaled"), '\\caption{Barrel ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.4\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{EndcapToT%s.png} \n'%(testname+"ibl_rescaled"), '\\caption{Endcap ToT}\n', '\\end{subfigure}\n', '\\end{figure*}\n', "\\clearpage\n"]
  printinfile(RescaledToT)
  printinfile(['\\end{landscape}\n'])

  scheme_def = ['\\section*{Changing ToT from 8 bit values to lower n bit values}\n', 'Two schemes have been used to convert 8 bit ToT to lower n bit ToT:\n', '\\begin{itemize}', '\\item {\\bf Scheme 1}: Every set of 16 consecutve ToT values converted to the floor of their mean, except 0 (8 bit) remains 0 (in 4 bit) eg: For 8 bit ToT to 4 bit ToT : 0 becomes 0, 1-15 becomes 7, 16-31 becomes 23, so on', '\\item {\\bf Scheme 2}: every set of consecutive $2^{(8-n)}$ values (starting from 0) are converted to their lowest value. eg: For 8 bit ToT to 4 bit ToT : 0-15 becomes 0, 16-31 becomes 16, so on', '\\end{itemize}', "\\clearpage\n"]
  printinfile(scheme_def)


  printinfile(['\\begin{landscape}\n'])
  ToTScheme1_2bit = ["\\section*{2bit ToT (Scheme 1) distribution}\n", "{\\footnotesize Scheme 1: Every 64 ToT values converted to the floor of their mean, except 0 remains 0 (0 becomes 0, 1-63 becomes 31, 64-127 becomes 95 and so on)}\n", "\\begin{figure*}[htb]\n", "\\centering\n", "\\begin{subfigure}[t]{0.3\\textwidth}\n", "\\centering\n", '\\includegraphics[width=\\linewidth]{IBLToT%s.png} \n'%(testname+"scheme1_2bit"), '\\caption{IBL ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.3\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{BarrelToT%s.png} \n'%(testname+"scheme1_2bit"), '\\caption{Barrel ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.3\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{EndcapToT%s.png} \n'%(testname+"scheme1_2bit"), '\\caption{Endcap ToT}\n', '\\end{subfigure}\n', '\\end{figure*}\n']

  printinfile(ToTScheme1_2bit)

  ToTScheme1_4bit = ["\\section*{4 bit ToT (scheme 1) distribution}\n", "{\\footnotesize Scheme 1: Every 64 ToT values converted to the floor of their mean, except 0 remains 0 (0 becomes 0, 1-63 becomes 31, 64-127 becomes 95 and so on)}\n", "\\begin{figure*}[htb]\n", "\\centering\n", "\\begin{subfigure}[t]{0.3\\textwidth}\n", "\\centering\n", '\\includegraphics[width=\\linewidth]{IBLToT%s.png} \n'%(testname+"scheme1_4bit"), '\\caption{IBL ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.3\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{BarrelToT%s.png} \n'%(testname+"scheme1_4bit"), '\\caption{Barrel ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.3\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{EndcapToT%s.png} \n'%(testname+"scheme1_4bit"), '\\caption{Endcap ToT}\n', '\\end{subfigure}\n', '\\end{figure*}\n']

  printinfile(ToTScheme1_4bit)
  printinfile(["\\clearpage\n"])
  printinfile(['\\end{landscape}\n'])

  printinfile(['\\begin{landscape}\n'])
  ToTScheme2_2bit = ["\\section*{2 bit ToT (scheme 2) distribution}\n", "{\\footnotesize Scheme 2: Every set of consecutive $2^{(8-n)}$ values (starting from 0) are converted to their lowest value. eg: For 8 bit ToT to 4 bit ToT : 0-15 becomes 0, 16-31 becomes 16, so on}\n", "\\begin{figure*}[htb]\n", "\\centering\n", "\\begin{subfigure}[t]{0.3\\textwidth}\n", "\\centering\n", '\\includegraphics[width=\\linewidth]{IBLToT%s.png} \n'%(testname+"scheme2_2bit"), '\\caption{IBL ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.3\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{BarrelToT%s.png} \n'%(testname+"scheme2_2bit"), '\\caption{Barrel ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.3\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{EndcapToT%s.png} \n'%(testname+"scheme2_2bit"), '\\caption{Endcap ToT}\n', '\\end{subfigure}\n', '\\end{figure*}\n']
          
  printinfile(ToTScheme2_2bit)

  ToTScheme2_4bit = ["\\section*{4 bit ToT (scheme 2) distribution}\n", "{\\footnotesize Scheme 2: Every set of consecutive $2^{(8-n)}$ values (starting from 0) are converted to their lowest value. eg: For 8 bit ToT to 4 bit ToT : 0-15 becomes 0, 16-31 becomes 16, so on}\n", "\\begin{figure*}[htb]\n", "\\centering\n", "\\begin{subfigure}[t]{0.3\\textwidth}\n", "\\centering\n", '\\includegraphics[width=\\linewidth]{IBLToT%s.png} \n'%(testname+"scheme2_4bit"), '\\caption{IBL ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.3\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{BarrelToT%s.png} \n'%(testname+"scheme2_4bit"), '\\caption{Barrel ToT}\n', '\\end{subfigure}\n', '\\begin{subfigure}[t]{0.3\\textwidth}\n', "\\centering\n", '\\includegraphics[width=\\linewidth]{EndcapToT%s.png} \n'%(testname+"scheme2_4bit"), '\\caption{Endcap ToT}\n', '\\end{subfigure}\n', '\\end{figure*}\n', "\\clearpage\n"]

  printinfile(ToTScheme2_4bit)
  printinfile(['\\end{landscape}\n'])

  Steps2ROC = ['\\section*{ROC Curves}\n', '\\begin{itemize}', '\\item The number NN used to initially get charge from the cluster pixels in the input set\n', '\\item Now the number NN was trained using ToT instead of charge in the input set\n', '\\item Number NN trained on 8 bit ToT was used to test the performance of 8 bit ToT test inputs, 4 bit ToT test inputs and 2 bit ToT test inputs\n', '\\item Finally, 4 bit ToT and 2 bit ToT events were tested with number NNs trained on 4 bit ToT and 2 bit ToT respectively\n', '\\end{itemize}', "\\clearpage\n"]

  printinfile(Steps2ROC)

  layers = ["ibl", "barrel", "endcap"]
  layers1 = ["IBL", "Barrel", "Endcap"]

  nscheme = 2 ##2 schemes of ToT digitization lowering - 1, 2

  for i in range(3):
    m = ((i%3)+1)
    n = (((i+1)%3)+1)
    heading = ['\\section*{%i and %i truth particle clusters}\n'%(m, n), "\\clearpage\n"]
    printinfile(heading)

    for layer in layers:
      title = ['\\section*{ROC\\_%ivs%i\\_%s\\_all}\n'%(m, n, layer)]
      printinfile(title)
      start_plots = ['\n','\\begin{figure*}[htb]\n']
      printinfile(start_plots)
      plot = ['\\begin{subfigure}[b]{0.475\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{OriginalToTtrained/scheme%i/ROC_%ivs%i_%s_all.png} \n'%(1, m, n, layer), '\\caption{scheme %i}\n'%(1), '\\end{subfigure}\n']
      printinfile(plot)
      printinfile(['\\hfill\n'])
      plot = ['\\begin{subfigure}[b]{0.475\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{OriginalToTtrained/scheme%i/ROC_%ivs%i_%s_all.png} \n'%(2, m, n, layer), '\\caption{scheme %i}\n'%(2), '\\end{subfigure}\n']
      printinfile(plot)
      printinfile(['\\vskip\\baselineskip\n'])
      plot = ['\\begin{subfigure}[b]{0.475\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{Selftrained/scheme%i/ROC_%ivs%i_%s_all.png} \n'%(1, m, n, layer), '\\caption{scheme %i}\n'%(1), '\\end{subfigure}\n']
      printinfile(plot)
      printinfile(['\\quad'])
      plot = ['\\begin{subfigure}[b]{0.475\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{Selftrained/scheme%i/ROC_%ivs%i_%s_all.png} \n'%(2, m, n, layer), '\\caption{scheme %i}\n'%(2), '\\end{subfigure}\n']
      printinfile(plot)
      end_plots = ['\n','\\end{figure*}\n', '\\clearpage\n']
      printinfile(end_plots)

    for layer1 in layers:
      title = ['\\section*{ROC\\_%ivs%i\\_%s\\_all}\n'%(n, m, layer1)]
      printinfile(title)
      start_plots = ['\n','\\begin{figure*}[htb]\n']
      printinfile(start_plots)
      plot = ['\\begin{subfigure}[b]{0.475\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{OriginalToTtrained/scheme%i/ROC_%ivs%i_%s_all.png} \n'%(1, n, m, layer1), '\\caption{scheme %i}\n'%(1), '\\end{subfigure}\n']
      printinfile(plot)
      printinfile(['\\hfill\n'])
      plot = ['\\begin{subfigure}[b]{0.475\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{OriginalToTtrained/scheme%i/ROC_%ivs%i_%s_all.png} \n'%(2, n, m, layer1), '\\caption{scheme %i}\n'%(2), '\\end{subfigure}\n']
      printinfile(plot)
      printinfile(['\\vskip\\baselineskip\n'])
      plot = ['\\begin{subfigure}[b]{0.475\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{Selftrained/scheme%i/ROC_%ivs%i_%s_all.png} \n'%(1, n, m, layer1), '\\caption{scheme %i}\n'%(1), '\\end{subfigure}\n']
      printinfile(plot)
      printinfile(['\\quad'])
      plot = ['\\begin{subfigure}[b]{0.475\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{Selftrained/scheme%i/ROC_%ivs%i_%s_all.png} \n'%(2, n, m, layer1), '\\caption{scheme %i}\n'%(2), '\\end{subfigure}\n']
      printinfile(plot)
      end_plots = ['\n','\\end{figure*}\n', '\\clearpage\n']
      printinfile(end_plots)

    #printinfile("\\footnotesize{TOT distribution with %i \\& %i clusters}"%(m, n))
    start_plot = ['\n','\\begin{figure*}[htb]\n']
    printinfile(start_plots)
    for layer2 in layers1:
      plot = ['\\begin{subfigure}[b]{0.3\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{%sToT_%ip%ip%s.png} \n'%(layer2, m, n, testname+"ibl_rescaled"), '\\caption{IBL rescaled}\n', '\\end{subfigure}\n']
      printinfile(plot)
    for layer2 in layers1:
      printinfile(['\\hfill\n'])
      plot = ['\\begin{subfigure}[b]{0.3\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{%sToT_%ip%ip%s.png} \n'%(layer2, m, n, testname+"scheme1_2bit"), '\\caption{Scheme1 2bit}\n', '\\end{subfigure}\n']
      printinfile(plot)
    for layer2 in layers1:
      printinfile(['\\hfill\n'])
      plot = ['\\begin{subfigure}[b]{0.3\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{%sToT_%ip%ip%s.png} \n'%(layer2, m, n, testname+"scheme2_2bit"), '\\caption{Scheme2 2bit}\n', '\\end{subfigure}\n']
      printinfile(plot)
    for layer2 in layers1:
      printinfile(['\\hfill\n'])
      #printinfile(['\\vskip\\baselineskip\n'])
      plot = ['\\begin{subfigure}[b]{0.3\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{%sToT_%ip%ip%s.png} \n'%(layer2, m, n, testname+"scheme1_4bit"), '\\caption{Scheme1 4bit}\n', '\\end{subfigure}\n']
      printinfile(plot)
    for layer2 in layers1:
      printinfile(['\\hfill\n'])
      plot = ['\\begin{subfigure}[b]{0.3\\textwidth}\n', '\\centering\n', '\\includegraphics[width=\\textwidth]{%sToT_%ip%ip%s.png} \n'%(layer2, m, n, testname+"scheme2_4bit"), '\\caption{Scheme2 4bit}\n', '\\end{subfigure}\n']
      printinfile(plot)
    end_plots = ["\\caption*{TOT distribution with %i \\& %i clusters}"%(m,n), '\n','\\end{figure*}\n', '\\clearpage\n']
    printinfile(end_plots)

  Conclusion = ['\\section*{Summary}\n', '\\begin{itemize}', '\\item After training for 4 and 2 bit ToTs, scheme 1 improved significantly atleast for 4 bit ToT, but scheme 2 didn\'t have much improvement\n', '\\item performance of scheme 2 is quite similar as both have same number of values, while the performance is a little different in the case of scheme 1, as there is an additional value (because zeros are retained). So, the number NN has translational symmetry in ToT values\n', '\\item for IBL, 2 bit and 4 bit ToT give same performance (even before retraining)\n', '\\item for barrel and endcap retraining with lower bit ToT helps\n', '\\end{itemize}', "\\clearpage\n"]

  printinfile(Conclusion)

  Actionitems = ['\\section*{Summary}\n', '\\begin{itemize}', '\\item Understand the reason behind nearly identical performance of 2 bit and 4 bit ToT at IBL\n', '\\item understand why ToT trained NN has better performance than charge trained NN in 2vs1 case\n', '\\item quantify the performance of number NN from ROC using Volume Under Surface and other methods\n', '\\item optimize the hyperparameters of number NN for lower bit ToT data set\n', '\\end{itemize}', "\\clearpage\n"]

  end = ['\\end{document}\n']
  printinfile(end)

  subprocess.call(['pdflatex', 'results.tex'])

if __name__ == '__main__':
     main()
