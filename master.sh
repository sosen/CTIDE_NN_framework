local_python #path to anaconda2 python
export NN=/afs/cern.ch/work/s/sosen/public/Qualification_Task/pixel-NN-training #LG's NN
export PATH=${NN}:${PATH}
export FRAMEHOME=${PWD}

#config file 'config.dat'
config="config.dat";

#read the list of conversion bits
nbits=$(echo `grep 'nbits' $config`| cut -f2 -d:);
nbits=${nbits//[[:blank:]]/};
IFS="," read -a nbits <<< "${nbits}";

echo ${nbits[@]}

##TRAIN DATA
if [ ! -d train_dir ]; then
  mkdir train_dir;
fi

# trained NNs would be stored here
if [ ! -d neuralNets ]; then
  mkdir neuralNets;
fi

#events to be used in the train data
ntrain=$(echo `grep 'trainNevents' $config`| cut -f2 -d:);
ntrain=${ntrain//[[:blank:]]/};
echo $ntrain;

#read the train input file from config file
traindata=$(echo `grep 'TrainData' $config`| cut -f2 -d:);
traindata=${traindata//[[:blank:]]/};
if [ ! -e "${FRAMEHOME}/numberNN_data/${traindata}" ]; then
  echo 'train data missing';
fi
if [ ! -e "train_dir/$traindata" ]; then
  cd ${FRAMEHOME}/numberNN_data;
  ln -s ${FRAMEHOME}/numberNN_data/${traindata} ${FRAMEHOME}/train_dir/${traindata};
fi
##process the train file ToTs
cd ${FRAMEHOME}/train_dir;
#ibl ToT times 16
root -l -q "${FRAMEHOME}/scripts/processToT.C(\"${traindata}\", \"${traindata%.*}.ibl_rescaled.root\", \"iblrescale\", ${ntrain})";
wait $!
#train with rescaled ibl + 8bit rest ToT
nohup python ${NN}/trainNN_keras.py \
        --training-input ${traindata%.*}.ibl_rescaled.root \
        --output 8bitTOT \
        --config <(python ${NN}/genconfig.py --type number) &
##
echo ${nbits[@]}
#reducing ToTs to nbits using schemes
for bit in "${nbits[@]}"
do
  for scheme in scheme1 scheme2;
  do
    echo converting train ToT to ${bit}bit using $scheme;
    root -l -q "${FRAMEHOME}/scripts/processToT.C(\"${traindata%.*}.ibl_rescaled.root\",\"${traindata%.*}.${scheme}_${bit}bit.root\", \"${scheme}\", -1, ${bit})";
    wait $!
    #train with converted ToT
nohup python ${NN}/trainNN_keras.py \
        --training-input ${traindata%.*}.${scheme}_${bit}bit.root \
        --output ${bit}bitTOT_${scheme} \
        --config <(python ${NN}/genconfig.py --type number) &
    ##
    PIDLAST=$!; #ends up with the last training pid, hoping it finishes last
    wait $PIDLAST;
  done
done
##

cd ${FRAMEHOME};

#wait $PIDLAST; #start testing step after training part is over, perhaps inneficient, improve later
echo `jobs`
##TEST DATA
if [ ! -d test_dir ]; then
  mkdir test_dir;
fi

#events to be used in the test data
ntest=$(echo `grep 'testNevents' $config`| cut -f2 -d:);
ntest=${ntest//[[:blank:]]/};
echo $ntest;

#read the test input file from config file
testdata=$(echo `grep 'TestData' $config`| cut -f2 -d:);
testdata=${testdata//[[:blank:]]/};
if [ ! -e "${FRAMEHOME}/numberNN_data/${testdata}" ]; then
  echo 'test data missing';
fi
if [ ! -e "test_dir/${testdata}" ]; then
  cd ${FRAMEHOME}/numberNN_data;
  ln -s ${FRAMEHOME}/numberNN_data/${testdata} ${FRAMEHOME}/test_dir/${testdata};
fi
cd ${FRAMEHOME}/test_dir;

#ibl ToT times 16
root -l -q "${FRAMEHOME}/scripts/processToT.C(\"${testdata}\", \"${testdata%.*}.ibl_rescaled.root\", \"iblrescale\", ${ntest})";
##testing with 8 bit ToT
if [ ! -d ${FRAMEHOME}/neuralNets/8bitTOT ]; then
  mkdir ${FRAMEHOME}/neuralNets/8bitTOT;
  mv ${FRAMEHOME}/train_dir/8bitTOT.* ${FRAMEHOME}/neuralNets/8bitTOT/;
fi
cd ${FRAMEHOME}/neuralNets/8bitTOT/;
if [ ! -d "8bitTOT" ]; then
  mkdir -p 8bitTOT;
fi
cd ${FRAMEHOME}/neuralNets/8bitTOT/8bitTOT/;
python ${NN}/evalNN_keras.py \
  --input ${FRAMEHOME}/test_dir/${testdata%.*}.ibl_rescaled.root \
  --model ${FRAMEHOME}/neuralNets/8bitTOT/8bitTOT.model.yaml \
  --weights ${FRAMEHOME}/neuralNets/8bitTOT/8bitTOT.weights.hdf5 \
  --normalization ${FRAMEHOME}/neuralNets/8bitTOT/8bitTOT.normalization.txt \
  --output 8bitTrained8bit.db \
  --config <(python ${NN}/genconfig.py --type number);
ln -s ${NN}/test-driver_sourav .; #may be unecessary, will check later
ln -s ${NN}/ROC .;
ln -s ${NN}/ROC_Graph .;
./test-driver_sourav number 8bitTrained8bit.db 8bitTrained8bit.root;
cd ${FRAMEHOME}/test_dir/;

#reducing ToTs to nbits using schemes
for bit in "${nbits[@]}"
do
  for scheme in scheme1 scheme2;
  do
    echo converting test ToT to ${bit}bit using ${scheme};
    root -l -q "${FRAMEHOME}/scripts/processToT.C(\"${testdata%.*}.ibl_rescaled.root\",\"${testdata%.*}.${scheme}_${bit}bit.root\", \"${scheme}\", -1, ${bit})";
    ##testing with 8 bit ToT
    cd ${FRAMEHOME}/neuralNets/8bitTOT/;
    mkdir -p ${scheme}/${bit}bitTOT;
    cd ${scheme}/${bit}bitTOT/;
python ${NN}/evalNN_keras.py \
  --input ${FRAMEHOME}/test_dir/${testdata%.*}.${scheme}_${bit}bit.root \
  --model ${FRAMEHOME}/neuralNets/8bitTOT/8bitTOT.model.yaml \
  --weights ${FRAMEHOME}/neuralNets/8bitTOT/8bitTOT.weights.hdf5 \
  --normalization ${FRAMEHOME}/neuralNets/8bitTOT/8bitTOT.normalization.txt \
  --output 8bitTrained${bit}bit.db \
  --config <(python ${NN}/genconfig.py --type number);
    ln -s ${NN}/test-driver_sourav .; #may be unecessary, will check later
    ln -s ${NN}/ROC .;
    ln -s ${NN}/ROC_Graph .;
    ./test-driver_sourav number 8bitTrained${bit}bit.db 8bitTrained${bit}bit.root;
    ##testing with $bit bit ToT
    mkdir -p ${FRAMEHOME}/neuralNets/${scheme}/${bit}bitTOT;
    cd ${FRAMEHOME}/neuralNets/${scheme}/${bit}bitTOT;
    mv ${FRAMEHOME}/train_dir/${bit}bitTOT_${scheme}.* ${FRAMEHOME}/neuralNets/${scheme}/${bit}bitTOT/;
python ${NN}/evalNN_keras.py \
  --input ${FRAMEHOME}/test_dir/${testdata%.*}.${scheme}_${bit}bit.root \
  --model ${bit}bitTOT_${scheme}.model.yaml \
  --weights ${bit}bitTOT_${scheme}.weights.hdf5 \
  --normalization ${bit}bitTOT_${scheme}.normalization.txt \
  --output ${bit}bitTrained${bit}bit.db \
  --config <(python ${NN}/genconfig.py --type number);
    ln -s ${NN}/test-driver_sourav .; #may be unecessary, will check later
    ln -s ${NN}/ROC .;
    ln -s ${NN}/ROC_Graph .;
    ./test-driver_sourav number ${bit}bitTrained${bit}bit.db ${bit}bitTrained${bit}bit.root;
    cd ${FRAMEHOME}/test_dir/;
    ##
  done
done
##

cd ${FRAMEHOME};

## plotting begins
##ROCs overlaying
chargeROCs=$(echo `grep 'extra_ROCs' config.dat`| cut -f2 -d:) #ROCs with charge as input
chargeROCs=${chargeROCs//[[:blank:]]/};
export PLOTS=${FRAMEHOME}/plot_dump;
export NETS=${FRAMEHOME}/neuralNets;
mkdir -p ${PLOTS}/OriginalToTtrained; #includes ROCs from tests on original ToT trained NN
mkdir -p ${PLOTS}/Selftrained; #includes ROCs with similar test and train sample pairs
commonIn="-f ${chargeROCs} -l Charge -f ${NETS}/8bitTOT/8bitTOT/8bitTrained8bit.root -l 8bitToT";
for scheme in scheme1 scheme2; do
  OriIn="${commonIn}";
  SelfIn="${commonIn}";
  cd ${NETS};
  for bit in `ls ${scheme}`; do
    OriIn="${OriIn} -f ${NETS}/8bitTOT/${scheme}/${bit}/8bitTrained${bit%TOT}.root -l ${bit%TOT}ToT";
    SelfIn="${SelfIn} -f ${NETS}/${scheme}/${bit}/${bit%TOT}Trained${bit%TOT}.root -l ${bit%TOT}ToT(trained)";
  done
  OriIn="${OriIn} --outdirname ${scheme}";
  SelfIn="${SelfIn} --outdirname ${scheme}";
  cd ${PLOTS}/OriginalToTtrained;
  python ${FRAMEHOME}/scripts/overlay.py ${OriIn};
  cd ${PLOTS}/Selftrained;
  python ${FRAMEHOME}/scripts/overlay.py ${SelfIn};
done
cd ${FRAMEHOME};

#commonIn="-f \"${chargeROCs}\" -l \"Charge\" -f \"${NETS}/8bitTOT/8bitTOT/8bitTrained8bit.root\" -l \"8bit ToT\"";
#for scheme in scheme1 scheme2; do
#  OriIn="--outdirname ${scheme} ${commonIn}";
#  SelfIn="--outdirname ${scheme} ${commonIn}";
#  cd ${NETS};
#  for bit in `ls ${scheme}`; do
#    OriIn="${OriIn} -f \"${NETS}/8bitTOT/${scheme}/${bit}/8bitTrained${bit%TOT}.root\" -l \"${bit%TOT} ToT\"";
#    SelfIn="${SelfIn} -f \"${NETS}/${scheme}/${bit}/${bit%TOT}Trained${bit%TOT}.root\" -l \"${bit%TOT} ToT(trained)\"";
#  done
#  cd ${PLOTS}/OriginalToTtrained;
#  python ${FRAMEHOME}/scripts/overlay.py ${OriIn};
#  cd ${PLOTS}/Selftrained;
#  python ${FRAMEHOME}/scripts/overlay.py ${SelfIn};
#done
#cd ${FRAMEHOME};
cd ${PLOTS};
for file in `ls ${FRAMEHOME}/test_dir/*.root`; do
  if [ "${file}" == "${FRAMEHOME}/test_dir/${testdata}" ]; then
    echo testdata;
    python ${FRAMEHOME}/scripts/testNbit.py "${file}" --ncluster ${ntest};
  else
    python ${FRAMEHOME}/scripts/testNbit.py "${file}" --ncluster ${ntest} --rescaledIBL;    
  fi
done

ln -s ${FRAMEHOME}/scripts/final_latex.py .
python final_latex.py #${testdata%.root}
cd ${FRAMEHOME}
